@ECHO OFF
taskkill /f /im php-cgi.exe
taskkill /f /im httpd.exe
ECHO Starting PHP FastCGI...
set PATH=.\PHP;%PATH%
.\RunHiddenConsole.exe .\PHP\php-cgi.exe -b 127.0.0.1:9123
ECHO Starting Apache Server...
start apache\bin\httpd.exe
pause