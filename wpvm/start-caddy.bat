@ECHO OFF
taskkill /f /im php-cgi.exe
taskkill /f /im caddy.exe
ECHO Starting PHP FastCGI...
set PATH=.\PHP;%PATH%
.\RunHiddenConsole.exe .\PHP\php-cgi.exe -b 127.0.0.1:9123
ECHO Starting Caddy Server...
caddy -conf="caddy.ini"
pause