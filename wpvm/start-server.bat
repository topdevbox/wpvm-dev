@ECHO OFF
CLS

:START
CLS
ECHO 1.Start apache
ECHO 2.Start Nginx
ECHO 3.Start CaddyServer
ECHO 4.Stop All
ECHO 5.Exit Console

CHOICE /C 12345 /M "Enter your choice:"
IF ERRORLEVEL 5 GOTO EXITCONSOLE
IF ERRORLEVEL 4 GOTO STOPALL
IF ERRORLEVEL 3 GOTO CADDY
IF ERRORLEVEL 2 GOTO NGINX
IF ERRORLEVEL 1 GOTO APACHE

:APACHE
call:CleanProcess
CLS
ECHO Starting PHP FastCGI...
set PATH=.\PHP;%PATH%
.\RunHiddenConsole.exe .\PHP\php-cgi.exe -b 127.0.0.1:9123
ECHO Starting Apache Server...
start apache\bin\httpd.exe
exit
GOTO START


:NGINX
call:CleanProcess
CLS
ECHO Starting PHP FastCGI...
set PATH=.\PHP;%PATH%
.\RunHiddenConsole.exe .\PHP\php-cgi.exe -b 127.0.0.1:9123
cd nginx
ECHO Starting Nginx Server...
start nginx.exe
GOTO START

:CADDY
call:CleanProcess
CLS
ECHO Starting PHP FastCGI...
set PATH=.\PHP;%PATH%
.\RunHiddenConsole.exe .\PHP\php-cgi.exe -b 127.0.0.1:9123
ECHO Starting Caddy Server...
start caddy -conf="caddy.ini"
exit
GOTO START

:STOPALL
CALL:CleanProcess
cls
GOTO START

:CleanProcess
taskkill /f /im php-cgi.exe
taskkill /f /im nginx.exe
taskkill /f /im httpd.exe
taskkill /f /im caddy.exe


:EXITCONSOLE
